"use strickt";

//  Теоретичні питання
//  1. Як можна створити рядок у JavaScript?

// Рядки можуть бути створені одинарними (''), подвійними ("") та зворотніми (``) лапками.

//  2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?

// Між одинарними ('') та подвійними ("") різниці немає. В зворотні можно вставляти любий вираз огортаючи його  ${…}

//  3. Як перевірити, чи два рядки рівні між собою?
// Для порівняння двох рядків, ми можемо використати оператор строгого дорівнювання:

const a = "learn";
const b = "today";

console.log(a === "learn"); // true
console.log(b === a); // false
// цей метод завжди враховує регістр.

const c = "javascript";
const d = "Javascript";

console.log(c === d); // false

// існує варіант порівняня строк без врахування регістра:

const c1 = "javascript";
const d1 = "Javascript";

console.log(c1.toLowerCase() === d1.toLowerCase()); // true

// Строки можна поріввняти за кількістю літер за допомогою оператора `length`:

const f = "javascript";
const j = "node.js";

console.log(f.length > j.length); // true

// Можливо перевірити чи входить зазначений текст в строку за допомогою оператора `includes`:

const m = "javascript";
const n = "python";

console.log(m.includes("script")); // true
console.log(n.includes("script")); // false

//  4. Що повертає Date.now()?

// Повертає поточний timestamp.

//  5. Чим відрізняється Date.now() від new Date()?

// Це семантично еквівалентно до new Date().getTime(), але не створює проміжного об’єкта Date. Цей підхід працює швидше.

// Практичні завдання
// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.
// Приклади паліндромів: дід, мадам.

function isPalindrome(inputStr) {
  let str = inputStr.toLowerCase();
  let strReversed = str.split("").reverse().join("");
  return str === strReversed;
}

console.log(isPalindrome(`мадам`));
console.log(isPalindrome(`дід`));

// 2. Створіть функцію, яка  перевіряє довжину рядка.
//Вона приймає рядок, який потрібно перевірити, максимальну довжину,
//повертає true, якщо рядок менше або дорівнює вказаній довжині,
// і false, якщо рядок довший.
//Ця функція стане в нагоді для валідації форми.Приклади використання функції:

// Рядок коротше 20 символів
// funcName('checked string', 20); // true

// Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false

function funcName(str, maxLength) {
  if (str.length <= maxLength) {
    return true;
  } else {
    return false;
  }
}
console.log(funcName("checked string", 20));
console.log(funcName("checked string", 10));

// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.

let birthday = prompt("Введіть дату народження: число / місяць /рік;");
let date = new Date();

function userBirthday(value) {
  let arrDate = value.split("/");
  arrDate[1] -= 1;
  birthday = new Date(arrDate[2], arrDate[1], arrDate[0]);
  if (
    birthday.getFullYear() == arrDate[2] &&
    birthday.getMonth() == arrDate[1] &&
    birthday.getDate() == arrDate[0]
  ) {
    birthday = (Date.now() - birthday) / (1000 * 60 * 60 * 24 * 365.25);

    return `Вам повних років ${Math.trunc(birthday)}.\n\nДата запросу: ${date}`;
  } else {
    alert("Введена вами дата некоректна!");
    return false;
  }
}
console.log(userBirthday(birthday));
